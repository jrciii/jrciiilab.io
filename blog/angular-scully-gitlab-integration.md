---
title: Angular + Scully + GitLab Pages Integration
description: How to build a static site with Angular, Scully and GitLab Pages
publish: true
date: 2020-02-28
---
# Angular + Scully + GitLab Pages Integration
### tl:dr; Use this .gitlab-ci.yml file in the root of your repo:

<div class="code-text-area">

```yaml
image: node:12.16.1

pages:
  cache:
    paths:
    - node_modules/

  stage: deploy
  script:
  - apt-get update
  - apt install -y gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
  - npm install -g @angular/cli@9.0.3
  - npm install
  - ng build --prod
  - chmod --recursive 777 .
  - adduser --disabled-password --gecos '' newuser
  - su newuser -c "npx scully --nw"
  - mkdir -p public
  - mv dist/static/* public/
  artifacts:
    paths:
    - public
  only:
  - master
  - pages
```
</div>

The build process, summarized:
- Trigger a new build on a GitLab runner (shared or otherwise)
- Use a node.js image for our build container
- Cache node_modules
- Update apt
- [Install dependencies for Chrome](https://techoverflow.net/2018/06/05/how-to-fix-puppetteer-error-while-loading-shared-libraries-libx11-xcb-so-1-cannot-open-shared-object-file-no-such-file-or-directory/)
- Install angular-cli
- Install node dependencies
- Build the angular project
- Run scully, in noWatch mode. We have to use a non-root user since running as root requires Chrome to be launched with --no-sandbox
- Move the static site files to public/

Get that going and bam, you've got a state of the art static site on your hands!

### Tech stack:
[Angular](https://angular.io), a platform for building mobile and desktop web applications  
[Scully](https://github.com/scullyio/scully), the best way to build the fastest Angular apps. Scully is a static site generator for Angular projects looking to embrace the JAMStack.  
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) is a feature that allows you to publish static websites directly from a repository in GitLab.