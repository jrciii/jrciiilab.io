fs = require('fs')
yaml = require('js-yaml')

fs.readdirSync("./comments/blog").forEach(dir => {
    fs.mkdirSync("./src/assets/comments/blog/" + dir, {recursive: true})
    fs.writeFileSync("./src/assets/comments/blog/" + dir + "/comments.json", 
    JSON.stringify(fs
            .readdirSync("./comments/blog/" + dir)
            .map(file => yaml.safeLoad(fs.readFileSync("./comments/blog/" + dir + "/" + file, "utf8")))))
})
