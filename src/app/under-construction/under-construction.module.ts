import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnderConstructionComponent } from './under-construction.component';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [    
    UnderConstructionComponent
  ],
  imports: [
    CommonModule,
    MatCardModule
  ],
  exports: [UnderConstructionComponent]
})
export class UnderConstructionModule { }
