import { Component, OnInit } from '@angular/core';
import { BlogDiscoveryService } from '../blog-discovery.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  /*blogs$ = this.scully.available$.pipe(
    map(routeList => routeList.filter((route: ScullyRoute) => route.route.startsWith(`/blog/`))),
    );
  
  constructor(private scully: ScullyRoutesService) {}*/
  blogs$ = this.bds.blogs$;
  constructor(private bds: BlogDiscoveryService) {}
  ngOnInit(): void {
   
  }
}
