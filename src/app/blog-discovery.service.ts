import { Injectable, OnInit } from '@angular/core';
import { ScullyRoutesService, ScullyRoute } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogDiscoveryService implements OnInit {
  ngOnInit(): void {
    /*this.blogs$.subscribe(blogs => {
      blogs.forEach(b => console.log(b));
    })*/
  }

  public blogs$ = this.scully.available$.pipe(
    map(routeList => routeList.filter((route: ScullyRoute) => route.route.startsWith(`/blog/`))),
    map(blogs => blogs.sort((a, b) => (a.date > b.date ? -1 : 1)))
    );

  constructor(private scully: ScullyRoutesService) { }
}
