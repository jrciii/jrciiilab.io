import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as marked from 'marked';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'
import { BlogComment} from './blogcomment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  createCommentsChain(commentTree, comment, level, parent) {
    const children = (commentTree[comment._id] || []);
    return [this.updateComment(comment, parent, level)].concat(children.map(c => this.createCommentsChain(commentTree, c, level + 1, comment)));
  }

  updateComment(comment, parent, level) {
    comment.parent = parent
    comment.level = level
    comment.verb = parent ? " replied to " + parent.name : "commented"
    comment.date = new Date(comment.date * 1000).toISOString()
    comment.message = marked(comment.message)
    return comment
  }

  public comments$(route: string): Observable<[BlogComment]> {
    return this.http.get<[BlogComment]>("/assets/comments" + route + "/comments.json")
    .pipe(
      map(comments => {
        const replyMap = comments.sort((a, b) => a.date - b.date)
          .reduce((tree, c) => {
            (tree[c.replyid] = tree[c.replyid] || []).push(c);
            return tree;
          }, new Map());
        const topLevel = replyMap["undefined"];
        const parsed = topLevel.map(c => this.createCommentsChain(replyMap, c, 0, null)).flat(Infinity);
        return parsed
      })
    )
  }
}
