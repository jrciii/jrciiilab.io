import { TestBed } from '@angular/core/testing';

import { BlogDiscoveryService } from './blog-discovery.service';

describe('BlogDiscoveryService', () => {
  let service: BlogDiscoveryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogDiscoveryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
