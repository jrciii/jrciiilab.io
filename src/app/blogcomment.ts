export class BlogComment {
    _id: string;
    name: string;
    email: string;
    url: string;
    date: number;
    message: string;
    replyid: string;
}