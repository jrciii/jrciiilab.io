import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogrollRoutingModule } from './blogroll-routing.module';
import { BlogrollComponent } from './blogroll.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [BlogrollComponent],
  imports: [
    CommonModule,
    BlogrollRoutingModule,
    MatButtonModule,
    MatCardModule,
    RouterModule,
    MatDividerModule
  ]
})
export class BlogrollModule { }
