import { Component, OnInit } from '@angular/core';
import { BlogDiscoveryService } from '../blog-discovery.service';

@Component({
  selector: 'app-blogroll',
  templateUrl: './blogroll.component.html',
  styleUrls: ['./blogroll.component.scss']
})
export class BlogrollComponent implements OnInit {
  blogs$ = this.bds.blogs$;
  JSON: any = JSON;
  constructor(private bds: BlogDiscoveryService) {  }

  ngOnInit(): void {
   
  }
}
