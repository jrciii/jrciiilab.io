import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogrollComponent } from './blogroll.component';

const routes: Routes = [{ path: '', component: BlogrollComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogrollRoutingModule { }
