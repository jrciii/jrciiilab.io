import {Renderer2, Component, ElementRef, AfterViewInit, QueryList, ViewChild, ViewChildren, Input} from '@angular/core';
import { MatToolbar } from '@angular/material/toolbar';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements AfterViewInit {
  @ViewChild('nav') nav: MatToolbar;

  constructor(
    private renderer: Renderer2
  ) {}

  ngAfterViewInit() {
    console.log(this.nav);
    this.renderer.setStyle(document.body, 'padding-top', this.nav._elementRef.nativeElement.offsetHeight + 'px');
  }
}
