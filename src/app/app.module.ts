import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSliderModule } from '@angular/material/slider';
import { NavModule } from './nav/nav.module';
import { UnderConstructionModule } from './under-construction/under-construction.module';
import { AppRoutingModule } from './app-routing.module';
import { ContactComponent } from './contact/contact.component';
import { ScullyLibModule, TransferStateService } from '@scullyio/ng-lib';
import { StickyHeaderComponent } from './sticky-header.component';
import { HomeModule } from './home/home.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    StickyHeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    NavModule,
    UnderConstructionModule,
    AppRoutingModule,
    ScullyLibModule.forRoot({useTranferState: true}),
    HomeModule
  ],
  providers: [TransferStateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
