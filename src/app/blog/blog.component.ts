import {Component, OnInit, ViewEncapsulation, ViewChild, ViewChildren} from '@angular/core';
import {ActivatedRoute, Router, ROUTES} from '@angular/router';
import {Location} from '@angular/common';
import { CommentsService } from '../comments.service';
import { isScullyGenerated, TransferStateService } from '@scullyio/ng-lib';
import { tap } from 'rxjs/operators';
import { MatCard } from '@angular/material/card';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated

})
export class BlogComponent implements OnInit {
  private replyVisibleMap: Map<string, boolean> = new Map<string, boolean>()
  @ViewChild('commentForm') commentForm;
  @ViewChildren(MatCard) replyForms;
  public hideReply(id: string) {
    this.replyVisibleMap.set(id, false)
  }

  public showReply(id: string) {
    this.replyVisibleMap.set(id, true)
  }

  public isVisible(id: string) {
    return this.replyVisibleMap.get(id)
  }

  commentHidden: boolean = true;

  comment(b: boolean) {
    this.commentHidden = !b;
  }

  slug(): string {
    return this.router.url.substring(1);
  }

  ngOnInit() {
    console.log("shat")
  }

  comments$ = isScullyGenerated() ? this.transferState.getState('comments') :
    this.commentsService.comments$(this.router.url).pipe(tap(comments => this.transferState.setState('comments', comments)))

  constructor(private router: Router, private route: ActivatedRoute, private _location: Location, private commentsService: CommentsService, private transferState: TransferStateService) {
  }

  backClicked() {
    this._location.back();
  }
  
  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }
}
