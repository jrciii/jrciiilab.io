import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ScullyLibModule, TransferStateService} from '@scullyio/ng-lib';
import {BlogRoutingModule} from './blog-routing.module';
import {BlogComponent} from './blog.component';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [BlogComponent],
  imports: [CommonModule,
    HttpClientModule,
    BlogRoutingModule,
    ScullyLibModule.forRoot({useTranferState: true}),
  MatCardModule,
MatDividerModule,
MatButtonModule]
})
export class BlogModule {}
