const {registerPlugin} = require('@scullyio/scully');
const fs = require('fs');
const util = require('util');
const yaml = require('js-yaml');
const marked = require('marked');

function createCommentsChain(commentTree, comment, level, parent, route) {
  const children = (commentTree[comment._id] || []);
  return createComment(comment, parent, level, route) + children.map(c => createCommentsChain(commentTree, c, level + 1, comment, route)).join("\n");
}

function createComment(comment, parent, level, route) {
  return `
  <div class="comment-div" style="padding: 8px; margin-left: ${level * 8}px;${parent ? " border-left: ridge;" : ""}">
  ${parent ? "" : "<hr>"}
  <h4>${comment.name} ${parent ? 'replied to ' + parent.name : 'commented'} on ${new Date(comment.date * 1000).toISOString()}</h4>
  ${comment.url ? "<h4>" + comment.name +"'s url: <a class=\"comment-url\" href=\"" + comment.url + "\">" + comment.url + "</a></h4>" : ""}
  <hr>
  ${marked(comment.message)}
  <button onclick="reply('${comment._id}')" class="reply-button" type="button">Reply to ${comment.name}!</button>
  <div class="reply-div">
  <form method="post" action="https://obscure-plateau-03682.herokuapp.com/v3/entry/gitlab/jrciii/jrciii.gitlab.io/master/comments" id="f.${comment._id}" style="display: none;">
  <input type="hidden" name="options[slug]" value="${route}">
  <input type="hidden" name="fields[replyid]" value="${comment._id}">
  <label for="fields[name]">Display name:</label>
  <input type="text" name="fields[name]" required><br/>
  <label for="fields[email]">Email (will be hashed, for Gravatar):</label>
  <input type="text" name="fields[email]"><br/>
  <label for="fields[url]">URL:</label>
  <input type="text" name="fields[url]"><br/>
  <label for="fields[message]">Message:</label><br/>
  <textarea name="fields[message]"></textarea><br/>
  <input type="submit" value="Submit">
  <button onclick="reply('${comment._id}')" class="hide-button" type="button">Hide</button>
  </form>
  </div>
  </div>
  `;
}

const hideScript = `
<script type="text/javascript">
  function reply(id) {
    let f = document.getElementById("f." + id);
    if (f.style.display == "none") {
      f.style.display = "block";
    } else {
      f.style.display = "none";
    }
  }
  </script>`;

function staticCommentsPlugin(html, route) {
  const rdp = util.promisify(fs.readdir);
  return rdp("./comments" + route.route)
    .then(files => {
      const comments = files
        .map(file => yaml.safeLoad(fs.readFileSync('./comments' + route.route + '/' + file, 'utf8')))
        .sort((a, b) => a.date - b.date)
        .reduce((tree, c) => {
          (tree[c.replyid] = tree[c.replyid] || []).push(c);
          return tree;
        }, new Map());
      
      const topLevel = comments["undefined"];
      const splitter = "<!--scullyContent-end-->";
      const htmlSplit = html.split(splitter);
      const commentsHtml = topLevel.map(c => createCommentsChain(comments, c, 0, null, route.route.substring(1))).join("");
      return htmlSplit[0] + commentsHtml + splitter + htmlSplit.slice(1).join("") + hideScript;
    })
    .catch(err => html);
}

const validator = async conf => [];

const StaticComments = 'staticComments';

registerPlugin('render', StaticComments, staticCommentsPlugin, validator);

module.exports.staticCommentsPlugin = staticCommentsPlugin;
module.exports.StaticComments = StaticComments;